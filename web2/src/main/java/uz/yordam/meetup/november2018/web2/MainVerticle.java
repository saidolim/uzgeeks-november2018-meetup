package uz.yordam.meetup.november2018.web2;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;

public class MainVerticle extends AbstractVerticle {

    @Override
    public void start() throws Exception {

        System.out.println("Starting Main Verticle");

        Future<String> httpVerticleDeployment = Future.future();

        vertx.deployVerticle(
                Web2Verticle.class,
                new DeploymentOptions().setInstances(4),
                httpVerticleDeployment.completer());
    }
}
