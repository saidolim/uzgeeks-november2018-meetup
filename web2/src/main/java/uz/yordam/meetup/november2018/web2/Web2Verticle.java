package uz.yordam.meetup.november2018.web2;

import io.vertx.core.AbstractVerticle;

public class Web2Verticle extends AbstractVerticle {

    static int couter = 0;
    int non_static = 0;

    public void start() {
        non_static = couter++;

        System.out.println("Starting Web in CPU" + non_static);

        vertx.createHttpServer().requestHandler(req -> {

            long a = toLong(req.getParam("a"));
            long b = toLong(req.getParam("b"));

            System.out.println("Result from CPU " + non_static + " a+b=" + (a + b));
            req.response()
                    .putHeader("content-type", "text/plain")
                    .end("I'm web in " + non_static + " a+b=" + (a + b));
        }).listen(8080);
    }

    public long toLong(String value) {
        long result = 0;
        try {
            result = Long.parseLong(value);
        } catch (Exception e) {
            result = 0;
        }
        return result;
    }
}
