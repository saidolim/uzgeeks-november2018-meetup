package uz.yordam.meetup

import io.vertx.core.AbstractVerticle
class KotlinServer : AbstractVerticle() {
    override fun start() {
        vertx.createHttpServer()
            .requestHandler { req ->
                req.response()
                    .putHeader("content-type", "text/plain")
                    .end("Hello from Vert.x - Kotlin")
            }.listen(8080)
    }
}